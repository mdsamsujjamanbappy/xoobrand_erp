@extends('layout.master')
@section('title','Role List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fas fa-list"></i> Role List</h4>
        </div>

        <div class="pagetitle-btn">
            @if(!empty($aclList[1][2]))
            <a href="{{route('role.create')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
            @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="myDatatable25" class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->info }}</td>
                            <td>{{ $role->createdBy->name ?? '' }}</td>
                            <td>{{ $role->updatedBy->name ?? '' }}</td>
                            <td>
                                @if($role->status == 1)
                                    <span class="text-primary">Active</span>
                                @else
                                    <span class="text-danger">Inactive</span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if(!empty($aclList[1][3]) || !empty($aclList[1][4]))
                                    <form method="post" action="{{ route('role.destroy', $role->id) }}">
                                        @if(!empty($aclList[1][3]))
                                            <a class="btn btn-sm btn-warning text-white" href="{{route('role.edit', $role->id)}}" title="Edit">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if(!empty($aclList[1][4]))
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-danger text-white delete" title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection