@extends('layout.master')
@section('title','Edit Activity Information')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Edit Activity Information </h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('activity.index')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-list"></i> Activity List</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                
                <form method="post" action="{{route("activity.update", $activity->id)}}">
                    @method('put')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control {{$errors->has("name") ? "is-invalid":""}}" id="name" name="name" placeholder="Enter Activity Name" value="{{old('name',$activity->name)}}">
                            <span class="text-danger"> {{$errors->has("name") ? $errors->first("name") : ""}} </span>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" placeholder="Enter Activity Description">{{old('description',$activity->description)}}</textarea>
                        </div>

                        <div class="form-group select2-parent">
                            <label>Status</label>
                            <select name="status" class="form-control single-select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <option value="0" {{(old("status") == 0 || $activity->status == 0 ) ? "selected" : "" }}>Inactive</option>
                                <option value="1" {{(old("status") == 1 || $activity->status == 1 ) ? "selected" : "" }}>Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-info">Update</button>
                        <button type="button" class="btn btn-default cancel">Cancel</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
