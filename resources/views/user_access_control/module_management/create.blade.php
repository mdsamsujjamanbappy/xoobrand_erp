@extends('layout.master')
@section('title','Add New Module')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fa fa-plus-circle"></i> Add New Module </h4>
        </div>

        <div class="pagetitle-btn">
            <a href="{{route('module.index')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-list"></i> Module List</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'module.store']) !!}
                    <div class="row">
                
                        <div class="col-sm-12 px-5">
                            <div class="form-group row">
                                <label for="form-label"><b>Module Name </b><span class="msb-txt-red">*</span></label>
                                <div class="col-sm-12 pl-0">
                                    <input type="text" class="form-control" name="name" autocomplete="off" placeholder="Module Name" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 px-5">
                            <div class="form-group row">
                                <label for="form-label"><b>Description</b></label>
                                <div class="col-sm-12 pl-0">
                                    <textarea class="form-control" name="description" rows="4" maxlength="250" autocomplete="off" placeholder="Description" ></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 px-4">
                            <div class="form-group">
                                <label><b>Module Activity</b> <span class="msb-txt-red">*</span></label>
                                @foreach($activites as $activity)
                                <div class="icheck-success">
                                    <input name="activities[]" type="checkbox" id="{{ $activity->id }}" value="{{ $activity->id }}">
                                    <label class="ml-3 font-weight-normal text-gray" for="{{ $activity->id }}">
                                        &nbsp; [{{$activity->id}}] {{$activity->name}}
                                    </label>
                                </div>
                                    @endforeach
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-12">
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection