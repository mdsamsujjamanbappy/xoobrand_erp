<div id="password_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update Password </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">

                        <div id="img_upload_div">
                        {!! Form::open(['method'=>'POST','route'=>'employee.employee_password_update']) !!}
                        <div class="row m-0 mt-3">
                            <div class="col-sm-9 m-auto">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">New Password <span style="color:red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="new_password" id="new_password" class="form-control pd-y-12" autocomplete="off" required="" placeholder="New Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Confirm New Password <span style="color:red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="confirm_new_password" id="confirm_new_password" class="form-control pd-y-12" autocomplete="off" required="" placeholder="Confirm New Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">
                                    <?php
                                        $fst = rand( 0, 9);
                                        $snd = rand( 0, 9);
                                        echo $fst ." + ". $snd;
                                        $sum = $fst+$snd;
                                     ?>
                                     = </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="security_system" hidden="" value="{{$sum}}" >
                                        <input type="text" name="security_user" class="form-control pd-y-12" autocomplete="off" required="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group text-center col-sm-12">
                            <input type="hidden" name="ref_id" value="{{($employee->id)}}">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Update Password</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
