<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="input-group hidden-xs-down wd-170 transition">
            <input id="searchbox" type="text" class="form-control" placeholder="Search">
            <span class="input-group-btn">
            <button class="btn btn-secondary" type="button"><i class="fas fa-search"></i></button>
          </span>
        </div><!-- input-group -->
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <?php
                $user_company = Auth::user()->company_id;
                if(isset($user_company)){
                    $company_info=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.id', 'asc')->first();
                }
            ?>
            <div class="dropdown">
                <a href="" class="nav-link pd-x-9 pd-t-15 pos-relative" data-toggle="dropdown">
                    @if(isset($company_info))
                        <span class="btn btn-teal btn-sm">{{$company_info->company_name}}</span>
                    @else
                        <span style="color: red;"><b>Select a company first.</b></span>
                    @endif
                    &nbsp;
                </a>
                @if(!empty($aclList[33][1]))
                <?php
                    $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.id', 'asc')->get(); 
                ?>
                <div class="dropdown-menu dropdown-menu-header">
                    <div class="media-list">
                        @foreach($company_list as $ci)
                        <a href="{{route('company_information.global_settings_company', base64_encode($ci->id))}}" onclick="return confirm('Are you sure to move?')" class="media-list-link read">
                            <div class="media">
                                <div class="media-body">
                                <p class="noti-text"><b>{{$ci->company_name}}</b></p>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div><!-- media-list -->
                </div><!-- dropdown-menu -->
                @endif

            </div><!-- dropdown -->

            <div class="dropdown">
                <?php 
                $notifications=0;
                ?>
                <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                    <i class="icon ion-ios-bell-outline tx-24"></i>
                    <!-- start: if statement -->

                   <?php if($notifications>0){ ?>
                        <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                   <?php } ?>

                    <!-- end: if statement -->
                </a>
                <div class="dropdown-menu dropdown-menu-header">
                    <div class="dropdown-menu-label">
                        <label>Notifications</label>
                        <a href="">Mark All as Read</a>
                    </div><!-- d-flex -->

                    <div class="media-list">
                        <!-- loop starts here -->

                       <a href="#" class="media-list-link read">
                            <div class="media">
                                <div class="media-body">
                                <p class="noti-text"></p>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-footer">
                            <!-- <a href=""><i class="fas fa-angle-down"></i> Show All Notifications</a> -->
                        </div>
                    </div><!-- media-list -->
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
            <div class="dropdown">
                 <?php 
                    $emp_photo = "default.png";
                    if(!empty(auth()->user()->ref_id)){
                        $employee = DB::table('tb_employee_list')
                        ->select('tb_employee_list.*')
                        ->where('tb_employee_list.id', '=', auth()->user()->ref_id)
                        ->first(); 
                        
                        if(!empty($employee->emp_photo)){
                            $emp_photo = $employee->emp_photo;
                        }
                    }else{
                        $emp_photo = "default.png";
                    }
                    ?>
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">{{auth()->user()->name}}</span>
                    <img src="{{asset('employee_profile_image/'.$emp_photo)}}" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img src="{{asset('employee_profile_image/'.$emp_photo)}}" class="wd-100" alt=""></a>
                        <h6 class="logged-fullname">{{auth()->user()->name}}</h6>
                        <p>{{auth()->user()->email}}</p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        <?php if(!empty(auth()->user()->ref_id)){ ?>
                            <li><a href="{{route('employee.my_profile')}}"><i class="icon ion-ios-person"></i> My Profile</a></li>
                        <?php } ?>
                        <!-- <li><a href=""><i class="icon ion-ios-gear"></i> Settings</a></li> -->
                        <li><a href="{{url('logout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- br-header-right -->
</div>