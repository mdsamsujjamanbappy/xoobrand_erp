@extends('layout.master')
@section('title','Employee Attendance Report')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Daily Attendance History (Date: {{date('d-m-Y', strtotime($request->attendance_date))}})</h4>
        </div>

        <div class="pagetitle-btn">

           {!! Form::open(['method'=>'POST','route'=>'employee.attendance.daily_attendance']) !!}
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group row">
                        <div class="col-sm-12 pl-0">
                            <input type="text" placeholder="Click here to select date" style="height: auto !important; padding: 2px !important;" value="{{$request->attendance_date}}" class="fc-datepicker form-control" name="attendance_date" autocomplete="off" required="" >
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group row">
                         <button class="btn btn-sm btn-teal" type="submit"> <i class="fa fa-search"></i> View</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <table id="datatable" class="table table-bordered display responsive">
                    <thead>
                        <tr>
                            <th style="text-align: center;">SL</th>
                            <th style="text-align: center;">Date</th>
                            <th style="text-align: center;">Day</th>
                            <th style="text-align: center;">Employee ID</th>
                            <th style="text-align: center;">Employee Name</th>
                            <th style="text-align: center;">Shift</th>
                            <!-- <th style="text-align: center;">Start Time</th> -->
                            <th style="text-align: center;">In Time</th>
                            <th style="text-align: center;">Late</th>
                            <!-- <th style="text-align: center;">End Time</th> -->
                            <th style="text-align: center;">Out Time</th>
                            <th style="text-align: center;">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach($attendance_data as $data)
                        <tr>
                            <td style="text-align: center;">{{++$i}}</td>
                            <td style="text-align: center;">{{$data['entry_date']}}</td>
                            <td style="text-align: center;">{{$data['day']}}</td>
                            <td style="text-align: center;">{{$data['employee_id']}}</td>
                            <td style="text-align: center;">{{$data['employee_name']}}</td>
                            <td style="text-align: center;">{{$data['shift']}}</td>
                            <!-- <td style="text-align: center;">{{$data['start_time']}}</td> -->
                            <td style="text-align: center;">
                            @if(!empty($data['in_time']))
                                {{date('h:i a', strtotime($data['in_time']))}}
                            @endif
                            </td>
                            <td style="text-align: center;">{{$data['late']}}</td>
                            <!-- <td style="text-align: center;">{{$data['end_time']}}</td> -->
                            <td style="text-align: center;">
                            @if(!empty($data['out_time'])&&($data['in_time']!=$data['out_time']))
                                {{date('h:i a', strtotime($data['out_time']))}}
                            @else

                                @if(!empty($data['in_time']))
                                    <span style="color:red;">Not Found</span>
                                @endif
                                
                            @endif
                            </td>
                            <td style="text-align: center;">
                                <?php 
                                    if ($data['status'] == 'Absent') {
                                        echo '<span style="background:red;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                    
                                    if ($data['status'] == 'Holiday') {
                                        echo '<span style="background:#ff22b1; color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                    
                                    if ($data['status'] == 'Present-Holiday') {
                                        echo '<span style="background:#009688;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                    
                                    if ($data['status'] == 'Late') {
                                        echo '<span style="background:#9c27b0;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present-Weekend') {
                                        echo '<span style="background:green;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Weekend') {
                                        echo '<span style="background:#00a8ff;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present') {
                                        echo '<span style="background:green;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Leave') {
                                        echo '<span style="background:#273c75;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>

                <div>

                </div>
            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            pageLength:25
        });

        // date picker
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'YYYY-MM-DD'
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
        
    });
</script>
@endsection
