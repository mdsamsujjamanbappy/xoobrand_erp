@extends('layout.master')

@section('title','Give Attendance')

@section('extra_css')
    {{--    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">--}}
@endsection
@section('content')
{{ Html::script('theme/js/sweetalert.min.js') }}
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Give Attendance</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <div class="row">
                    <div class="col-sm-12">

                        <?php $today= date('Y-m-d'); ?>
                        <h3 style='text-align: center;'>Name: <b>{{Auth::user()->name}}</b></h3>
                        <h3 style='text-align: center;'>Date: <b>{{date('d-m-Y', strtotime($today))}}</b></h3>
                        <hr>
                        <?php 

                        $emp_ref_id = Auth::user()->ref_id;
                        $employee_id = DB::table("tb_employee_list")->select('employee_id')->where('tb_employee_list.id', '=', $emp_ref_id)->first();
                        $check = DB::table("tb_attendance_history")->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $today]])->count(); 
                        if($check==0){
                        ?>
                        {!! Form::open(['method'=>'POST','route'=>'employee.attendance.give_attendance_by_app.checkin']) !!}
                            <?php 
                            $ipaddress = '';
                            if (isset($_SERVER['HTTP_CLIENT_IP'])){
                                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                            }
                            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            }
                            else if(isset($_SERVER['HTTP_X_FORWARDED'])){
                                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                            }
                            else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
                                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                            }
                            else if(isset($_SERVER['HTTP_FORWARDED'])){
                                $ipaddress = $_SERVER['HTTP_FORWARDED'];
                            }
                            else if(isset($_SERVER['REMOTE_ADDR'])){
                                $ipaddress = $_SERVER['REMOTE_ADDR'];
                            }
                            else{
                                $ipaddress = 'UNKNOWN';
                            }
                            ?>

                            <input type="" hidden value="<?php echo $ipaddress; ?>" name="check_in_ip">
                            <input type="" hidden id="latitude" name="check_in_lat">
                            <input type="" hidden id="longitude"  name="check_in_long">
                           <center> <button class="btn btn-success btn-lg" name="check_in_btn" type="submit"> Check In </button></center>
                           <br>
                        {!! Form::close() !!}
                        <?php }else{

                            $checkinData = DB::table("tb_attendance_history")->where([['emp_id', '=', $employee_id->employee_id], ['attendance_date', '=', $today]])->first(); 

                            echo "<h4 style='color: green; text-align: center;'>Today Checked In: ". date('h:i a', strtotime($checkinData->in_time))."</h4><br />";
                        
                            if(!empty($checkinData->out_time)){
                                echo "<h4 style='color: blue; text-align: center;'>Last Checked Out: ". date('h:i a', strtotime($checkinData->out_time))."</h4>";
                            }
                            echo "<hr />";
                        } ?>

                        {!! Form::open(['method'=>'POST','route'=>'employee.attendance.give_attendance_by_app.checkout']) !!}
                        <?php 
                            $ipaddress = '';
                            if (isset($_SERVER['HTTP_CLIENT_IP'])){
                                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                            }
                            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            }
                            else if(isset($_SERVER['HTTP_X_FORWARDED'])){
                                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                            }
                            else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
                                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                            }
                            else if(isset($_SERVER['HTTP_FORWARDED'])){
                                $ipaddress = $_SERVER['HTTP_FORWARDED'];
                            }
                            else if(isset($_SERVER['REMOTE_ADDR'])){
                                $ipaddress = $_SERVER['REMOTE_ADDR'];
                            }
                            else{
                                $ipaddress = 'UNKNOWN';
                            }
                            ?>

                            <input hidden type="" value="<?php echo $ipaddress; ?>" name="check_out_ip">
                            <input hidden type="" id="latitude" name="check_out_lat">
                            <input hidden type="" id="longitude"  name="check_out_long">
                            <center><button class="btn btn-primary btn-lg" name="check_out_btn" type="submit"> Check Out </button></center>
                        {!! Form::close() !!}
                           <br>
                           <br>

                        <center><button class="btn btn-default btn-lg"  onclick="reload()" type="submit"> Refresh </button></center>


                    </div>
                </div>

            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection

@section('extra_js')

<script>
    $(document).ready(function(){
        
        if($().select2) {
            $('#employee_id').select2({
                minimumResultsForSearch: '',
                placeholder: "Select a Employee"
            });
        }

        // date picker
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'YYYY-MM-DD'
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
        
        function getLocation() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
          } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
          }
        }

        function showPosition(position) {
            document.getElementById("latitude").value = position.coords.latitude;
            document.getElementById("longitude").value = position.coords.longitude;
        }

        getLocation();

    });

    function reload() {
        location.reload()
    }
</script>
@endsection