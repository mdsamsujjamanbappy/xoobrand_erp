$(function(){
  'use strict';
	
	$('input[type=number]').on('mousewheel', function(e) {
		  $(e.target).blur();
	});
		
    $('#myDatatable').DataTable();

    $('.myDatatable').DataTable();

	$('#myDatatable25').DataTable({
        pageLength: 25
    });

    $('#myDatatable50').DataTable({
        pageLength: 50
    });
    
    $('input').attr('autocomplete', 'off');

    if($().select2) {  
        $('.mySelect2').select2({
            placeholder: "-- Select --"
        });

        $('.mySelect2ModalAdd').select2({
            minimumResultsForSearch: '',
            placeholder: "-- Select --",
            dropdownParent: $('#add_modal .modal-content')
        });

        $('.mySelect2ModalUpdate').select2({
            minimumResultsForSearch: '',
            placeholder: "-- Select --",
            dropdownParent: $('#edit_modal .modal-content')
        });
    }

    $('.delete').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        swal({
            title: "Are you sure to destroy?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
         },
         function(isConfirm){
           if (isConfirm){
               form.submit();
            } else {
              swal("Cancelled", "Your information is safe :)", "error");
                 e.preventDefault();
            }
         });
    });

    // date picker
    $('.myDatepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'YYYY-MM-DD'
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });

	// date picker
    $('.myDatepicker1').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'YYYY-MM-DD'
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });

    // date picker
    $('.myDatepicker2').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'YYYY-MM-DD'
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    
    // date picker
    $('.myDatepicker3').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'YYYY-MM-DD'
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });

    function reload() {
        location.reload()
    }
});
