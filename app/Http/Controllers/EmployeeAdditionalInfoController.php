<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeAdditionalInfoController extends Controller
{
  	public function employee_additional_info_store(Request $request)
  	{
	  	if ($request->hasFile('attachment')) {
	      $attachment = $request->ref_id."_".time().rand(0,9).'.'.$request->attachment->getClientOriginalExtension();
	      $request->attachment->move('employee_additional_info', $attachment);
	    }else{
	    	 $attachment = NULL;
	    }

	    $employee_nominee = DB::table('tb_employee_others_info')->insert([
	      'emp_id'        	   	=>  $request->ref_id,
	      'title'      			=>  $request->title,
	      'description' 		=>  $request->description,
	      'attachment'   		=>  $attachment,
	      'created_by'         	=>  Auth::user()->id,
	      'created_at'          =>  Carbon::now()->toDateTimeString(),
	      'updated_at'         	=>  Carbon::now()->toDateTimeString()
	    ]);

	    Session::flash('successMessage','Employee additional information has been successfully added.');
	    return redirect()->back();
  	}

	public function employee_additional_info_destroy($id)
	{   
		$id=base64_decode($id);
		$employee_others_info = DB::table('tb_employee_others_info')->where('id', '=', $id)->first();
		$employee_others = DB::table('tb_employee_others_info')->where('id', '=', $id)->delete();
		
		if(!empty($employee_others)){
	    	if(!empty($employee_others_info->attachment)){
	            if($employee_others_info->attachment != "default.png"){
	              $file_path = "employee_additional_info"."/".$employee_others_info->attachment;
	              if(file_exists($file_path)){
	                unlink($file_path);
	              }
	            }
	        }
	    }

		Session::flash('successMessage','Employee additional information has been successfully destroyed.');
	    return redirect()->back();
	}
	
}
