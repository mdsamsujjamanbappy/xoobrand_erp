<?php

namespace App\Http\Controllers;

use Session;
use App\Model\Activity;
use App\MyClass\OwnLibrary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ActivityController extends Controller
{
    protected $moduleId = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        OwnLibrary::validateAccess($this->moduleId, 1);
        $activities = Activity::with('createdBy')->orderBy("id")->get();
        return view('user_access_control.activity_management.list', compact('activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        OwnLibrary::validateAccess($this->moduleId, 2);
        return view('user_access_control.activity_management.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        OwnLibrary::validateAccess($this->moduleId, 2);
        $rules = [
            "name" => "required"
        ];

        $message = [
            "name.required" => "Name is required",
        ];

        $validation = Validator::make($request->all(), $rules, $message);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        } else {

            $activity = new Activity();

            $activity->name        = $request->name;
            $activity->description = $request->description;
            $activity->created_by  = 1;
            $activity->updated_by  = 1;

            if ($activity->save()) {
                Session::flash('successMessage','New activity has been successfully created.');
                return redirect()->route("activity.index");
            } else {
                Session::flash('failedMessage','New activity has been failed to create.');
                return redirect()->back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        OwnLibrary::validateAccess($this->moduleId, 3);
        return view("user_access_control.activity_management.edit", compact("activity"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Model\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        OwnLibrary::validateAccess($this->moduleId, 3);
        $rules = [
            "name" => 'required'
        ];

        $message = [];

        $validation = Validator::make($request->all(), $rules, $message);

        if ($validation->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validation);
        } else {
            $activity->name        = $request->name;
            $activity->description = $request->description;
            $activity->status      = $request->status;

            if ($activity->save()) {
                Session::flash('successMessage','Activity has been successfully Updated.');
                return redirect()->route("activity.index");
            } else {
                Session::flash('failedMessage','Activity has been failed to Update.');
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        OwnLibrary::validateAccess($this->moduleId, 4);
        if ($activity->forceDelete()) {
            session()->flash("success", "Activity Deleted");
            Session::flash('successMessage','Activity has been successfully destroyed.');
            return redirect()->back();
        } else {
            Session::flash('failedMessage','Activity has been failed to destroye.');
            return redirect()->back();
        }
    }
}
