<?php

namespace App\Http\Controllers\Auth;

use DB;
use Carbon\Carbon;
use App\Model\User;
use App\Model\ModuleToUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MSBLoginController extends Controller
{
    public function login_view(){
        return view('auth.login');
    }

    public function login(Request $request){

        $rules =[
            'email' => 'email|required',
            'password' => 'required'
        ];
        $message = [];

        $validation = Validator::make($request->all(),$rules,$message);

        if ($validation->fails()){
            return redirect()->back()
                ->withErrors($validation)
                ->withInput($request->except('password'));
        }

        $user = array(
            'email' => $request->email,
            'password' => $request->password,
            'status' => 1
        );

        $remember_me = $request->has('remember') ? true : false;

        if (Auth::attempt($user,$remember_me)) {
			$dataArr = ModuleToUser::where('user_id', Auth::user()->id)->get();
            $accessArr = array();
            if (!empty($dataArr)) {
                foreach ($dataArr as $item) {
                    $accessArr[$item->module_id][$item->activity_id] = $item->activity_id;
                }
            }
			$ipaddress = $this->ip_address();
            $str = DB::table('users')->where('id', auth()->id())->update([
                'login_ip'         =>  $ipaddress,
                'last_login_at'    =>  Carbon::now(),
            ]);
            Session::put('acl', $accessArr);
            return Redirect::to('/dashboard');
        } else {
            Session::flash('error', 'Your username or password was incorrect.!');
            return redirect()->back()->withInput($request->except('password'));
        }
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return Redirect::route('login.view');
    }

    public static function ip_address(){
    	$ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])){
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        }
        else if(isset($_SERVER['REMOTE_ADDR'])){
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }
        else{
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }
}
