<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeEmploymentController extends Controller
{
  	public function employee_employment_store(Request $request)
  	{
	  	if ($request->hasFile('wh_attachment')) {
	      $wh_attachment = $request->ref_id."_".time().rand(0,9).'.'.$request->wh_attachment->getClientOriginalExtension();
	      $request->wh_attachment->move('employee_employment', $wh_attachment);
	    }else{
	    	 $wh_attachment = NULL;
	    }

	    $employee_employment = DB::table('tb_employee_work_history')->insert([
	      'emp_id'        	 =>  $request->ref_id,
	      'wh_company_name'  =>  $request->wh_company_name,
	      'wh_designation'   =>  $request->wh_designation,
	      'wh_joining_date'  =>  $request->wh_joining_date,
	      'wh_resign_date'   =>  $request->wh_resign_date,
	      'wh_description'   =>  $request->wh_description,
	      'wh_attachment'    =>  $wh_attachment,
	      'created_by'       =>  Auth::user()->id,
	      'created_at'       =>  Carbon::now()->toDateTimeString(),
	      'updated_at'       =>  Carbon::now()->toDateTimeString()
	    ]);

	    Session::flash('successMessage','Employee employment information has been successfully added.');
	    return redirect()->back();
  	}

	public function employee_employment_destroy($id)
	{   
		$id=base64_decode($id);
		$employee_work_history_info = DB::table('tb_employee_work_history')->where('id', '=', $id)->first();
		$employee_work_history = DB::table('tb_employee_work_history')->where('id', '=', $id)->delete();
		
		if(!empty($employee_work_history)){
	    	if(!empty($employee_work_history_info->wh_attachment)){
	            if($employee_work_history_info->wh_attachment != "default.png"){
	              $file_path = "employee_employment"."/".$employee_work_history_info->wh_attachment;
	              if(file_exists($file_path)){
	                unlink($file_path);
	              }
	            }
	        }
	    }

		Session::flash('successMessage','Employee employment information has been successfully destroyed.');
	    return redirect()->back();
	}
}
