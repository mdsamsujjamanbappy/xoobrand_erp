<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyAccountCurrencyController extends Controller
{
    public function currency_setup()
    {   
        $user_company = Auth::user()->company_id;
        if(empty(Auth::user()->company_id)){
             Session::flash('failedMessage','Please select a company to access account module.');
            return view('company_information.company_selector_view', compact('company_list'));
        }
        
        $currency_info = DB::table('tb_company_account_currency')->where([['company_id', $user_company], ['status', 1]])->first();
        
        return view('accounts.currency_info.setup', compact('currency_info'));

    }

    public function currency_setup_update(Request $request)
    {   
        $user_company = Auth::user()->company_id;
        if(empty(Auth::user()->company_id)){
             Session::flash('failedMessage','Please select a company to access account module.');
            return view('company_information.company_selector_view', compact('company_list'));
        }

        if(isset($request->id)){
			$str = DB::table('tb_company_account_currency')->where('id', $request->id)->update([
	            'currency_code'			=>	$request->currency_code,
	            'prefix'				=>	$request->prefix,
	            'suffix'				=>	$request->suffix,
	            'decimal_point_number'	=>	$request->decimal_point_number,
	            'company_id'			=>	Auth::user()->company_id,
	            'status'				=>	1,
	      		'created_by'   			=> 	Auth::user()->id,
	            'updated_at'			=>	Carbon::now()->toDateTimeString()
	        ]);
             Session::flash('successMessage','Company accounts currency has been successfully updated.');
        	return redirect()->back();
        }else{
        	$str = DB::table('tb_company_account_currency')->insert([
	            'currency_code'			=>	$request->currency_code,
	            'prefix'				=>	$request->prefix,
	            'suffix'				=>	$request->suffix,
	            'decimal_point_number'	=>	$request->decimal_point_number,
	            'company_id'			=>	Auth::user()->company_id,
	            'status'				=>	1,
	      		'created_by'   			=> 	Auth::user()->id,
	            'created_at'			=>	Carbon::now()->toDateTimeString(),
	            'updated_at'			=>	Carbon::now()->toDateTimeString()
	        ]);
             Session::flash('successMessage','Company accounts currency has been successfully created.');
        	return redirect()->back();
        }
    }
}