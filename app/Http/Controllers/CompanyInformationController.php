<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyInformationController extends Controller
{
    public function company_information_list()
    {   
        $company_info = DB::table('tb_company_information')->get();
        return view('company_information.company_information_list',compact('company_info'));
    }


    public function store(Request $request)
    {
        if ($request->hasFile('company_logo')) {
            $logoName = time().'.'.$request->company_logo->getClientOriginalExtension();
            $request->company_logo->move(('company_logo'),$logoName);
        }else{
            $logoName='default.png';
        }

        $str = DB::table('tb_company_information')->insert([
            'company_name'		=>	$request->company_name,
            'company_tagline'	=>	$request->company_tagline,
            'company_phone'		=>	$request->company_phone,
            'company_email'		=>	$request->company_email,
            'company_address1'	=>	$request->company_address1,
            'company_address2'	=>	$request->company_address2,
            'company_logo'		=>	$logoName,
            'status'			=>	$request->status,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Company Information has been successfully added.');
        return redirect()->back();
    }

    public function destroy($id)
    {   
        $id=base64_decode($id);
        $count = DB::table('tb_employee_list')->where('company_id', '=', $id)->count();

        if($count>0){
            Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
            $company_info = DB::table('tb_company_information')->where('id', '=', $id)->delete();
            Session::flash('successMessage','Company Information has been successfully destroyed.');
        }
        return redirect()->back();
    }


    public function edit($id)
    {   
    	$id=base64_decode($id);
    	$company_info = DB::table('tb_company_information')->where('id', '=', $id)->first();
        return response()->json($company_info);
    }


    public function update(Request $request)
    {
        if ($request->hasFile('company_logo')) {
            $logoName = time().'.'.$request->company_logo->getClientOriginalExtension();
            $request->company_logo->move(('company_logo'),$logoName);
           
            try {
            	unlink('company_logo/'.$request->edit_company_logo);
			}catch (\Exception $e) {
			}

        }else{
            $logoName=$request->edit_company_logo;
        }
         $companyUpdate=DB::table('tb_company_information')->where('id',$request->id)->update([
            'company_name'		=>	$request->company_name,
            'company_tagline'	=>	$request->company_tagline,
            'company_phone'		=>	$request->company_phone,
            'company_email'		=>	$request->company_email,
            'company_address1'	=>	$request->company_address1,
            'company_address2'	=>	$request->company_address2,
            'company_logo'		=>	$logoName,
            'status'			=>	$request->status,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

         Session::flash('successMessage','Company Information has been successfully updated.');
         return redirect()->back();
    }

    public function details($id)
    {   
    	$id=base64_decode($id);
    	$company_info = DB::table('tb_company_information')->where('id', '=', $id)->first();
        return response()->json($company_info);
    }


    public function company_selector_view()
    {   
        $company_list = DB::table('tb_company_information')->get();
        if(empty(Auth::user()->company_id)){
             Session::flash('failedMessage','Please select a company to access software.');
        }
        return view('company_information.company_selector_view', compact('company_list'));
    }

    public function global_settings_company($id)
    {   
        $id = base64_decode($id);
        $str = DB::table('users')->where('users.id', Auth::user()->id)->update([
            'company_id'     => $id,
            'updated_at'     => Carbon::now()->toDateTimeString()
        ]);

         Session::flash('successMessage','Company Information has been successfully configured.');
         return redirect()->back();
    }

}
