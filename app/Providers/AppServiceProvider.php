<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('money', function ($amount) {
            return "<?php echo '৳' . number_format($amount, 2); ?>";
        });

        Blade::directive('dollarMoney', function ($amount) {
            return "<?php echo '$' . number_format($amount, 2); ?>";
        });

        Blade::directive('numberFormatZeroDecimal', function ($amount) {
            return "<?php echo number_format($amount); ?>";
        });

        Blade::directive('quantity', function ($quantity) {
            return "<?php echo ($quantity); ?>";
        });
        
    }
}
