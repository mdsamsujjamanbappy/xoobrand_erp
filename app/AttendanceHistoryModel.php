<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceHistoryModel extends Model
{
    protected $table ='tb_attendance_history_tmp';
    protected $fillable = [
        'emp_id', 'attendance_date', 'punch_time', 'created_at', 'updated_at',
    ];
}
