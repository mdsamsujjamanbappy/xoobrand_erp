<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GenderListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_gender_list')->insert([
            [  
                'id'                => 1,
                'gender_name'   => "Male",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 2,
                'gender_name'   => "Female",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 3,
                'gender_name'   => "Other",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}
