<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CompanyListTableSeeder::class,
            DepartmentListTableSeeder::class,
            DesignationListTableSeeder::class,
            GenderListTableSeeder::class,
            WeekendHolidayTableSeeder::class,
        ]);
    }
}
