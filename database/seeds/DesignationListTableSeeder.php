<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DesignationListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_designation_list')->insert([
            [  
                'id'                => 1,
                'designation_name'  => "Manager",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 2,
                'designation_name'  => "Asst. Manager",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 3,
                'designation_name'  => "Quality Controller",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}
