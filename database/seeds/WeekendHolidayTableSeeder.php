<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WeekendHolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_weekend_holiday')->insert([
            [
                'id'            => 1,
                'day_name'      => 'Saturday',
                'status'        => 1,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 2,
                'day_name'      => 'Sunday',
                'status'        => 0,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 3,
                'day_name'      => 'Monday',
                'status'        => 0,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 4,
                'day_name'      => 'Tuesday',
                'status'        => 0,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 5,
                'day_name'      => 'Wednesday',
                'status'        => 0,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 6,
                'day_name'      => 'Thursday',
                'status'        => 0,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],
            [
                'id'            => 7,
                'day_name'      => 'Friday',
                'status'        => 1,
                'company_id'    => 1,
                'created_at'    =>Carbon::now()->toDateTimeString(),
                'updated_at'    =>Carbon::now()->toDateTimeString()
            ],

        ]);
    }
}
