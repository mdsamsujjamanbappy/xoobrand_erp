<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseQuotationListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_product_purchase_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('purchase_number', 30)->index()->nullable();
            $table->string('supplier_id', 20)->index()->nullable();
            $table->date('purchase_date')->nullable();
            $table->string('total_amount', 20)->nullable();
            $table->string('discount_value', 8)->nullable();
            $table->tinyinteger('discount_method')->nullable();
            $table->text('remarks')->nullable();
            $table->tinyinteger('company_id')->index()->nullable();
            $table->string('created_by', 4)->nullable();
            $table->tinyinteger('status')->default('1')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_purchase_quotation_list');
    }
}
