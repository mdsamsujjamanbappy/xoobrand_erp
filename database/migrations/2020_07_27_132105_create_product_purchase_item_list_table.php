<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseQuotationItemListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_product_purchase_item_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('purchase_ref_id', 30)->index()->nullable();
            $table->string('product_id', 8)->nullable();
            $table->string('product_price', 12)->nullable();
            $table->string('product_qty', 8)->nullable();
            $table->string('item_discount_value', 8)->nullable();
            $table->tinyinteger('item_discount_method')->nullable();
            $table->string('item_discount_amount', 8)->nullable();
            $table->string('sub_total_amount', 20)->nullable();
            $table->tinyinteger('company_id')->index()->nullable();
            $table->string('created_by', 4)->nullable();
            $table->tinyinteger('status')->default('1')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_purchase_quotation_item_list');
    }
}
