<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeaveApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_leave_application', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('unique_id')->nullable();
            $table->bigInteger('employee_id')->index()->nullable();
            $table->bigInteger('leave_type_id')->index()->nullable();
            $table->date('leave_starting_date')->nullable();
            $table->date('leave_ending_date')->nullable();
            $table->integer('actual_days')->nullable();
            $table->tinyInteger('approved_by')->nullable();
            $table->string('attachment')->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->string('created_by', 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_leave_application');
    }
}
