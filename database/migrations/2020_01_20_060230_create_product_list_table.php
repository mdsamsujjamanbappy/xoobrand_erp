<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_product_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name', 255)->nullable();
            $table->string('product_barcode', 20)->nullable();
            $table->text('product_description')->nullable();
            $table->string('product_location', 30)->nullable();
            $table->string('product_category_id', 8)->nullable();
            $table->string('product_unit_id', 8)->nullable();
            $table->string('product_brand_id', 8)->nullable();
            $table->string('minimum_stock', 8)->nullable();
            $table->string('product_price', 8)->nullable();
            $table->string('sale_price', 8)->nullable();
            $table->string('product_size', 8)->nullable();
            $table->date('product_expiry_date')->nullable();
            $table->text('product_photo')->nullable();
            $table->tinyInteger('company_id')->index()->nullable();
            $table->string('created_by', 8)->nullable();
            $table->tinyInteger('status')->default('1')->comment('1 for Active, 0 for Inactive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_product_list');
    }
}
