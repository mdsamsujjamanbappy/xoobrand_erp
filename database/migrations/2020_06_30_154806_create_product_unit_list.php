<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductUnitList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_product_unit_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unit_name', 255)->nullable();
            $table->text('unit_description')->nullable();
            $table->tinyInteger('company_id')->index()->nullable();
            $table->string('created_by', 8)->nullable();
            $table->tinyInteger('status')->default('1')->comment('1 for Active, 0 for Inactive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_product_unit_list');
    }
}
